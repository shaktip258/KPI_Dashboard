﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using KPI_DASHBOARD.Core.Data;
using KPI_DASHBOARD.Core.Business;
using PagedList;
using System.Linq;
using static KPI_DASHBOARD.Core.Business.Helper;
using KPI_DASHBOARD.Security;

namespace KPI_DASHBOARD.Controllers
{
    [AuthorizeRole((int)ApplicationRole.Analyst)]
    [OutputCache(NoStore = true, Duration = 0)]
    public class AnalystController : Controller
    {
        private IAnalystManager analystManager;

        public AnalystController()
        {
            analystManager = new AnalystManager();
        }

        public AnalystController(IAnalystManager _analystManager)
        {
            analystManager = _analystManager;
        }

        // GET: (System) Analyst
        public ActionResult Index(int? page, int? userID)
        {
            //int pageSize = 5;
            //int pageIndex = 1;
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            //EMPLOYEE_LOGIN user = analystManager.FindUser(userID);
            //List<EMPLOYEE_LOGIN> list = new List<EMPLOYEE_LOGIN>();
            //list.Add(user);

            ViewBag.id = userID;

            return View();
        }

        // GET: (System) Analyst/Manage
        public ActionResult Manage(int? page)
        {
            int pageSize = 5;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            List<EMPLOYEE_LOGIN> userList = analystManager.GetUserList();
            return View(userList.ToPagedList(pageIndex, pageSize));
        }

        // GET: (System) Analyst/Create
        public ActionResult Create()
        {
            ViewBag.EMP_ROLE = new SelectList(analystManager.GetRoles(), "ROLE_ID", "ROLE_NAME");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EMP_LOG_USER_SYS_ID,EMP_LOG_PASSWORD,EMP_ROLE")] EMPLOYEE_LOGIN empDetails)
        {
            if (ModelState.IsValid)
            {
                analystManager.CreateUser(empDetails);
                return RedirectToAction("Manage");
            }

            ViewBag.EMP_ROLE = new SelectList(analystManager.GetRoles(), "ROLE_ID", "ROLE_NAME", empDetails.EMP_ROLE);
            return View(empDetails);
        }

        // GET: (System) Analyst/Details
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPLOYEE_LOGIN empDetails = analystManager.FindUser(id);
            if (empDetails == null)
            {
                return HttpNotFound();
            }
            return View(empDetails);
        }

        // GET: (System) Analyst/Edit
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPLOYEE_LOGIN empDetails = analystManager.FindUser(id);
            if (empDetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.EMP_ROLE = new SelectList(analystManager.GetRoles(), "ROLE_ID", "ROLE_NAME", empDetails.EMP_ROLE);
            return View(empDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EMP_LOG_USER_SYS_ID,EMP_LOG_PASSWORD,EMP_ROLE")] EMPLOYEE_LOGIN empDetails)
        {
            if (ModelState.IsValid)
            {
                analystManager.EditUser(empDetails);
                return RedirectToAction("Manage");
            }
            ViewBag.EMP_ROLE = new SelectList(analystManager.GetRoles(), "ROLE_ID", "ROLE_NAME", empDetails.EMP_ROLE);
            return View(empDetails);
        }

        // GET: (System) Analyst/Delete
        public ActionResult Delete(int? id, int? RoleId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //if (RoleId == (int)ApplicationRole.Analyst)
            //{
            //    return View("Error");
            //}
            EMPLOYEE_LOGIN empDetails = analystManager.FindUser(id);
            if (empDetails == null)
            {
                return HttpNotFound();
            }
            return View(empDetails);
        }

        // POST: (System) Analyst/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            analystManager.DeleteUser(id);
            return RedirectToAction("Manage");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                analystManager.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult HRMasterData()
        {
            return View();
        }

        public ActionResult Kenexa()
        {
            return View();
        }

        public ActionResult Terminations()
        {
            return View();
        }

        public ActionResult Absences()
        {
            return View();
        }
    }
}
