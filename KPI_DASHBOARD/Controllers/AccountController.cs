﻿using System.Web.Mvc;
using KPI_DASHBOARD.Core.Business;
using KPI_DASHBOARD.Core.Data;
using System.Web.Routing;

namespace KPI_DASHBOARD.Controllers
{
    public class AccountController : Controller
    {
        IAccountManager loginManager;
        IAuthentication authenticationHelper;

        public AccountController()
        {
            loginManager = new AccountManager();
            authenticationHelper = new FormsAuth();

        }

        public AccountController(IAccountManager _loginManager, IAuthentication _authenticationHelper)
        {
            loginManager = _loginManager;
            authenticationHelper = _authenticationHelper;
        }

        //GET: this action is called for all anonymous users to get authenticated
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                string controllerRole = Session["LoggedInUserRole"].ToString();
                return RedirectToAction("Index", controllerRole);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //POST: Authenticate the user and redirect to action based on its role
        [HttpPost]
        public ActionResult Login(EMPLOYEE_LOGIN loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var userId = loginManager.ValidateUser(loginViewModel.EMP_LOG_USER_SYS_ID, loginViewModel.EMP_LOG_PASSWORD);
                if (userId != 0)
                {
                    authenticationHelper.SetAuthCookie(loginViewModel.EMP_LOG_USER_SYS_ID);
                    Session.Add("LoggedInUser", userId);
                    string controllerRole = loginManager.GetUserRole(userId) == null ? "Home" : loginManager.GetUserRole(userId);
                    Session.Add("LoggedInUserRole", controllerRole);
                    return RedirectToAction("Index", controllerRole, routeValues: new { userID = userId });
                }
                else
                {
                    ModelState.AddModelError("", "The user login or password provided is incorrect.");
                }

            }
            // If we got this far, something failed, redisplay form
            return View(loginViewModel);
        }

        // POST: /Account/LogOff
        [HttpPost]
        [Authorize]
        public ActionResult LogOff()
        {
            authenticationHelper.LogOff();
            return RedirectToAction("Index", "Home");
        }

    }
}
