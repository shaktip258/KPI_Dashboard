﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using KPI_DASHBOARD.Core.Data;
using KPI_DASHBOARD.Core.Business;
using KPI_DASHBOARD.Security;
using PagedList;

namespace KPI_DASHBOARD.Controllers
{
    [AuthorizeRole((int)Helper.ApplicationRole.Reporter)]
    [OutputCache(NoStore = true, Duration = 0)]
    public class ReporterController : Controller
    {
        // GET: Reporter
        public ActionResult Index(int? page, int? userID)
        {
            ViewBag.id = userID;

            return View();
        }
    }
}
