﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_DASHBOARD.Controllers
{
    public class TableauController : Controller
    {
        public ActionResult HRMasterData()
        {
            return View();
        }

        public ActionResult Kenexa()
        {
            return View();
        }

        public ActionResult Terminations()
        {
            return View();
        }

        public ActionResult Absences()
        {
            return View();
        }
    }
}