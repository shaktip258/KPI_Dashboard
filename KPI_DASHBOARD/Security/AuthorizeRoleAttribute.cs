﻿using KPI_DASHBOARD.Core.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPI_DASHBOARD.Security
{
    public class AuthorizeRoleAttribute : AuthorizeAttribute
    {
        private readonly int userAssignedRoles;
        public AuthorizeRoleAttribute(int role)
        {
            this.userAssignedRoles = role;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;

            IAccountManager accountManager = new AccountManager();
            authorize = accountManager.IsUserInRole(int.Parse(httpContext.User.Identity.Name), userAssignedRoles);
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new ViewResult { ViewName = "Error" };
        }
    }
}