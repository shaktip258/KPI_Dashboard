﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using KPI_DASHBOARD.Core.Data;
using System.Linq;
using System.Web;

namespace KPI_DASHBOARD.Core.Data
{
    [MetadataType(typeof(EMPLOYEE_LOGINMetadata))]
    class EMLOYEE_LOGIN
    {
        public class EMPLOYEE_LOGINMetadata
        {
            [Required]
            [Display(Name = "User System ID")]
            public int EMP_LOG_USER_SYS_ID { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string EMP_LOG_PASSWORD { get; set; }

            [Display(Name = "Role")]
            public Nullable<int> EMP_ROLE { get; set; }

            [ForeignKey("EMP_ROLE")]
            public virtual USER_ROLES UserRoles { get; set; }
        }
    }
}
