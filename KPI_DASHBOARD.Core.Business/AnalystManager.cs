﻿using KPI_DASHBOARD.Core.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_DASHBOARD.Core.Business
{
    public interface IAnalystManager
    {
        void DeleteUser(int empID);

        void EditUser(EMPLOYEE_LOGIN empDetails);

        void CreateUser(EMPLOYEE_LOGIN empDetails);

        List<EMPLOYEE_LOGIN> GetUserList();

        DbSet<USER_ROLES> GetRoles();

        EMPLOYEE_LOGIN FindUser(int? empID);

        List<EMPLOYEE_LOGIN> GetUserByEmpID(int empID);

        void Dispose();
    }

    /// <summary>
    /// The below class will implement IAdminManger
    /// </summary>

    public class AnalystManager : IAnalystManager
    {
        private KPIDashboardEntities dbContext;

        public AnalystManager()
        {
            this.dbContext = new KPIDashboardEntities();
        }

        public AnalystManager(KPIDashboardEntities _dbContext)
        {
            this.dbContext = _dbContext;
        }

        /// <summary>
        /// This method will delete the user 
        /// </summary>
        /// <param name="empID"></param>
        public void DeleteUser(int empID)
        {
            try
            {
                EMPLOYEE_LOGIN empLogin = dbContext.EMPLOYEE_LOGIN.SingleOrDefault(u => u.EMP_LOG_USER_SYS_ID == empID);
                if (empLogin != null)
                {
                    dbContext.EMPLOYEE_LOGIN.Remove(empLogin);
                }

                dbContext.SaveChanges();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// This method update the user details 
        /// </summary>
        /// <param name="EMPLOYEE_LOGIN"></param>
        public void EditUser(EMPLOYEE_LOGIN empDetails)
        {
            var empAccount = dbContext.EMPLOYEE_LOGIN.Find(empDetails.EMP_LOG_USER_SYS_ID);
            dbContext.Entry(empAccount).CurrentValues.SetValues(empDetails);
            dbContext.Entry(empAccount).State = EntityState.Modified;
            dbContext.SaveChanges();
        }


        /// <summary>
        /// This is use to Create new user
        /// </summary>
        /// <param name="EMPLOYEE_LOGIN"></param>
        public void CreateUser(EMPLOYEE_LOGIN empDetails)
        {
            dbContext.EMPLOYEE_LOGIN.Add(empDetails);
            dbContext.SaveChanges();
        }

        public List<EMPLOYEE_LOGIN> GetUserList()
        {
            List<EMPLOYEE_LOGIN> userList = dbContext.EMPLOYEE_LOGIN.Include(u => u.USER_ROLES).ToList();
            return userList;
        }

        public EMPLOYEE_LOGIN FindUser(int? empID)
        {
            EMPLOYEE_LOGIN user = dbContext.EMPLOYEE_LOGIN.Find(empID);
            return user;
        }

        public DbSet<USER_ROLES> GetRoles()
        {
            return dbContext.USER_ROLES;
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }

        public List<EMPLOYEE_LOGIN> GetUserByEmpID(int empID)
        {
            return dbContext.EMPLOYEE_LOGIN.Where(u => u.EMP_LOG_USER_SYS_ID.Equals(empID)).ToList();
        }
    }
}
