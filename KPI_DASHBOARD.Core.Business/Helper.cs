﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI_DASHBOARD.Core.Business
{
    public class Helper
    {
        public enum ApplicationRole
        {
            Analyst = 1,
            Reporter = 2,
            Manager = 3,
            Executive = 4
        }
    }
}
