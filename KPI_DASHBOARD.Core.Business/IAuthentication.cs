﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace KPI_DASHBOARD.Core.Business
{
    public interface IAuthentication
    {
        void SetAuthCookie(int empID);
        void LogOff();
    }

    public class FormsAuth : IAuthentication
    {
        public void SetAuthCookie(int empID)
        {
            FormsAuthentication.SetAuthCookie(empID.ToString(), false);
        }

        public void LogOff()
        {
            FormsAuthentication.SignOut();
        }
    }
}
