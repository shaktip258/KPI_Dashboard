﻿using System.Linq;
using KPI_DASHBOARD.Core.Data;
using System;

namespace KPI_DASHBOARD.Core.Business
{
    public interface IAccountManager
    {
        int ValidateUser(int empID, string password);
        string GetUserRole(int empID);
        bool IsUserInRole(int empID, int roleID);
    }

    public class AccountManager : IAccountManager
    {
        private KPIDashboardEntities dbContext;

        public AccountManager()
        {
            this.dbContext = new KPIDashboardEntities();
        }
        public AccountManager(KPIDashboardEntities _dbContext)
        {
            this.dbContext = _dbContext;
        }
        /// <summary>
        /// This method will validated the user credentials against the database
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int ValidateUser(int empID, string password)
        {
            String userPass = dbContext.EMPLOYEE_LOGIN.Where(o => o.EMP_LOG_USER_SYS_ID.Equals(empID) && o.EMP_LOG_PASSWORD.Equals(password)).Select(u => u.EMP_LOG_PASSWORD).SingleOrDefault();
            var userId = dbContext.EMPLOYEE_LOGIN.Where(o => o.EMP_LOG_USER_SYS_ID.Equals(empID) && o.EMP_LOG_PASSWORD.Equals(password)).Select(u => u.EMP_LOG_USER_SYS_ID).SingleOrDefault();
            if (string.Equals(password, userPass, StringComparison.CurrentCulture))
                return userId;
            else
                return 0;
        }

        /// <summary>
        /// This method returns the role of the user 
        /// </summary>
        /// <param name="userId"></param>
        public string GetUserRole(int empID)
        {
            var userRole = (from roles in dbContext.USER_ROLES
                            join user in dbContext.EMPLOYEE_LOGIN on roles.ROLE_ID equals user.EMP_ROLE
                            where user.EMP_LOG_USER_SYS_ID == empID
                            select roles.ROLE_NAME).SingleOrDefault();
            return userRole;
        }

        /// <summary>
        /// Method will check is the user is in the role
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public bool IsUserInRole(int empID, int RoleID)
        {
            int? roleID = dbContext.EMPLOYEE_LOGIN.Where(o => o.EMP_LOG_USER_SYS_ID.Equals(empID)).Select(u => u.EMP_ROLE).FirstOrDefault();

            if (RoleID == roleID)
            {
                return true;
            }

            return false;
        }
    }
}